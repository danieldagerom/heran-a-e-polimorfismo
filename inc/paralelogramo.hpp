#ifndef PARALELOGRAMO_HPP
#define PARALELOGRAMO_HPP

#include "formageometrica.hpp"

class Paralelogramo : public FormaGeometrica{
   public:
     Paralelogramo(float base, float altura);

     // Sobrescrita de Método
     float calculaArea();
     float calculaPerimetro();

};


#endif

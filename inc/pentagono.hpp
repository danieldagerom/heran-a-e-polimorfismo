#ifndef PENTAGONO_HPP
#define PENTAGONO_HPP

#include "formageometrica.hpp"
#include <math.h>

class Pentagono : public FormaGeometrica{
   public:
     Pentagono(float base);

     // Sobrescrita de Método
     float calculaArea();
     float calculaPerimetro();

};


#endif

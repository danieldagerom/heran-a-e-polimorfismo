#ifndef HEXAGONO_HPP
#define HEXAGONO_HPP

#include "formageometrica.hpp"
#include <math.h>

class Hexagono : public FormaGeometrica{
   public:
     Hexagono(float base);

     // Sobrescrita de Método
     float calculaArea();
     float calculaPerimetro();

};

#endif

#ifndef QUADRADO_HPP
#define QUADRADO_HPP

#include "formageometrica.hpp"

class Quadrado : public FormaGeometrica{
   public:
     Quadrado(float base, float altura);

     // Sobrescrita de Método
     float calculaArea();
     float calculaPerimetro();

};


#endif

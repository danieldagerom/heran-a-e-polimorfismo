#ifndef CIRCULO_HPP
#define CIRCULO_HPP

#include "formageometrica.hpp"

class Circulo : public FormaGeometrica{
   public:
     Circulo(float altura);

     // Sobrescrita de Método
     float calculaArea();
     float calculaPerimetro();

};


#endif

#include "paralelogramo.hpp"

Paralelogramo::Paralelogramo(float base, float altura){
    setTipo("Paralelogramo");
    setLados(4);
    setBase(base);
    setAltura(altura);
}

float Paralelogramo::calculaArea(){

   return getBase()*getAltura();
}
float Paralelogramo::calculaPerimetro(){
   return 2*(getBase() + getAltura());
}

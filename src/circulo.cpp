#include "circulo.hpp"

#define PI 3.14159

Circulo::Circulo(float altura){
    setTipo("Circulo");
    //setLados(3);
    //setBase(base);
    setAltura(altura);
}

float Circulo::calculaArea(){
   return PI * (getAltura()/2) * (getAltura()/2);
}
float Circulo::calculaPerimetro(){
   return PI * getAltura();
}

#include "quadrado.hpp"

Quadrado::Quadrado(float base, float altura){
    setTipo("Quadrado");
    setLados(4);
    setBase(base);
    setAltura(altura);
}

float Quadrado::calculaArea(){
   return getBase()*getAltura();
}
float Quadrado::calculaPerimetro(){
   return 4*getBase();
}

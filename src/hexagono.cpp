#include "hexagono.hpp"


Hexagono::Hexagono(float base){
    setTipo("Hexagono");
    setLados(6);
    setBase(base);
    //setAltura(altura);
}

float Hexagono::calculaArea(){
   return 6 * (getBase() * getBase()) * sqrt(3) / 4;
}
float Hexagono::calculaPerimetro(){
   return 6*getBase();
}

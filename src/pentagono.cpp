#include "pentagono.hpp"


Pentagono::Pentagono(float base){
    setTipo("Pentagono");
    setLados(5);
    setBase(base);
    //setAltura(altura);
}

float Pentagono::calculaArea(){
   return sqrt(25 + 10 * sqrt(5)) / 4 * (getBase() * getBase());
}
float Pentagono::calculaPerimetro(){
   return 5*getBase();
}
